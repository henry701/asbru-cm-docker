# TODO

## Quality Of Life improvements

- Deconvolute script options and flow: For new users this current mess of different settings and included bash files is just not usable

## Chocolatey Package

- Dependencies:
  - [WSL2](https://community.chocolatey.org/packages/wsl2)
  - [Docker Desktop](https://community.chocolatey.org/packages/docker-desktop)
  - [X Server](https://community.chocolatey.org/packages/vcxsrv)
    - Check for already-running X-Server or other binaries before installing? Possible?
- Ensure stuff is started before running docker binary on runscript:
  - Docker Desktop should be up
  - X Server should be up
- Create Shortcut to runscript and place on all user's desktops (common user desktop)
