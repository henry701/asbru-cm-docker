# Known Issues

## No/Small Impact

### pgrep: unrecognized option: c

This is used by Ásbrú code to get the desktop environment. Since Busybox's pgrep implementation does not support "-c" option (return match count instead of matches) and also does not exit with a specific code for syntax errors (1 is used both for "not found" and for "invalid syntax"), this is not reliably fixable from Ásbrú's side. Considering the current usage, this error is safely ignorable.
