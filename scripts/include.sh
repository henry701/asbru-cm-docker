#!/bin/bash

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Default is run, of course
source "${SCRIPT_DIR}/setup_run.sh"

# Your (user) settings here! ;)
if [[ -f ~/asbru-cm-docker-include.sh ]]; then
    source ~/asbru-cm-docker-include.sh
fi
