#!/bin/bash

# Optimized for pushing image to docker hub

DOCKER_IMAGE_TAG="latest"

DOCKER_BUILD_IMAGE="1"
DOCKER_BUILD_SQUASH="1"
DOCKER_RUN="0"
DOCKER_PUSH="1"
