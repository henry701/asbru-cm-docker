#!/bin/bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/ ;)
set -euo pipefail
IFS=$'\n\t'

# Too much output can't hurt, it's Bash.
set -x

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# For setting defauls depending on environment
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     runningUnder=linux;;
    Darwin*)    runningUnder=darwin;;
    CYGWIN*)    runningUnder=cygwin;;
    MINGW*)     runningUnder=mingw;;
    *)          runningUnder="UNKNOWN:${unameOut}"
esac

# Stop path munging especially on docker commands...
if [[ "$runningUnder" == "mingw" ]]; then
    export MSYS_NO_PATHCONV=1
    export MSYS2_ARG_CONV_EXCL="*"
fi

INCLUDE_FILE="${INCLUDE_FILE-include.sh}"
if [[ ! -z "${INCLUDE_FILE}" && -f "${INCLUDE_FILE}" ]]; then
    source "${INCLUDE_FILE}"
else
    DIRED_INCLUDE_FILE="${SCRIPT_DIR}/${INCLUDE_FILE}"
    if [[ -f "${DIRED_INCLUDE_FILE}" ]]; then
        source "${DIRED_INCLUDE_FILE}"
    fi
fi

DOCKERFILE_PATH="${DOCKERFILE_PATH-}"
if [[ -z "${DOCKERFILE_PATH}" ]]; then
    DOCKERFILE_PATH="$(dirname $(find . -name Dockerfile -type f | head -1))"
fi

if [[ -z ${WSL_INTEROP_DISTRO+x} ]]; then
    if [[ "$runningUnder" == "mingw" || "$runningUnder" == "cygwin" ]]; then
        WSL_INTEROP_DISTRO="$(wsl --list | sed 's/\x00//g' | sed 's/\r//g' | grep ' (Default)' | sed 's/ (Default)//g')"
    else
        WSL_INTEROP_DISTRO=""
    fi
fi

if [[ -z ${USE_WSLG+x} ]]; then
    # Check for Windows
    if [[ "$runningUnder" == "mingw" || "$runningUnder" == "cygwin" ]]; then
        # Check for wslg existance
        if [[ -e "\\\\wsl\$\\${WSL_INTEROP_DISTRO}\\mnt\\wslg" ]]; then
            # Check for integrated docker support (needed for forwarding WSLG sockets and associated dirs)
            WSL_DOCKER_TEST_EXIT_CODE=0
            wsl --distribution "${WSL_INTEROP_DISTRO}" -- command -v docker || WSL_DOCKER_TEST_EXIT_CODE=$?
            if [[ "${WSL_DOCKER_TEST_EXIT_CODE}" -eq 0 ]]; then
                USE_WSLG=1
            else
                USE_WSLG=0
            fi
        else
            USE_WSLG=0
        fi
    else
        USE_WSLG=0
    fi
fi

DOCKER_IMAGE_TARGET="${DOCKER_IMAGE_TARGET-runner}"

DOCKER_IMAGE_NAME="${DOCKER_IMAGE_NAME-asbru-cm}"
DOCKER_IMAGE_TAG="${DOCKER_IMAGE_TAG-local}"

DOCKER_IMAGE_FULL_NAME="${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}"

DOCKER_CONTAINER_NAME="${DOCKER_CONTAINER_NAME-asbru-cm}"
DOCKER_CONTAINER_ENVFILE="${DOCKER_CONTAINER_ENVFILE-.container.env}"

DOCKER_CONTAINER_PRESERVE="${DOCKER_CONTAINER_PRESERVE-0}"
DOCKER_CONTAINER_EXTRA_ARGS="${DOCKER_CONTAINER_EXTRA_ARGS-}"
DOCKER_CONTAINER_MOUNT_DRIVES="${DOCKER_CONTAINER_MOUNT_DRIVES-0}"

DOCKER_BUILD_IMAGE="${DOCKER_BUILD_IMAGE-1}"
DOCKER_BUILD_EXTRA_ARGS="${DOCKER_BUILD_EXTRA_ARGS-}"
DOCKER_BUILD_SQUASH="${DOCKER_BUILD_SQUASH-0}"

DOCKER_START="${DOCKER_START-1}"

DOCKER_PUSH="${DOCKER_PUSH-0}"
DOCKER_REGISTRY="${DOCKER_REGISTRY-registry.hub.docker.com}"

if [[ "${DOCKER_BUILD_SQUASH}" == "1" ]]; then
    DOCKER_BUILD_EXTRA_ARGS="${DOCKER_BUILD_EXTRA_ARGS} --squash"
fi

FORCE_EXEC_DOCKER_INSIDE_WSL=0

# Display needs this because of some hard-coded bullshit in mingw's /etc/profile.d/env.sh that sets DISPLAY to "needs-to-be-defined"
DISPLAY="${DISPLAY-needs-to-be-defined}"
if [[ -z "${DISPLAY}" || "${DISPLAY}" == "needs-to-be-defined" ]]; then
    if [[ "$runningUnder" == "cygwin" || "$runningUnder" == "mingw" ]]; then
        if [[ "${USE_WSLG}" == "1" ]]; then
            FORCE_EXEC_DOCKER_INSIDE_WSL=1
            DISPLAY="$(wsl --distribution ${WSL_INTEROP_DISTRO} -- echo '${DISPLAY}')"
            DOCKER_CONTAINER_EXTRA_ARGS="${DOCKER_CONTAINER_EXTRA_ARGS} -v '/tmp/.X11-unix:/tmp/.X11-unix' -v '/mnt/wslg:/mnt/wslg' -e WAYLAND_DISPLAY="'"'"$(wsl --distribution ${WSL_INTEROP_DISTRO} -- echo '${WAYLAND_DISPLAY}')"'"'" -e XDG_RUNTIME_DIR="'"'"$(wsl --distribution ${WSL_INTEROP_DISTRO} -- echo '${XDG_RUNTIME_DIR}')"'"'""
        else
            DISPLAY="$(/c/Windows/System32/route.exe print | grep 0.0.0.0 | head -1 | awk '{print $4}'):0.0"
        fi
    else
        DISPLAY=":0"
    fi
fi

PULSE_SERVER="${PULSE_SERVER-}"
if [[ -z "${PULSE_SERVER}" ]]; then
    if [[ "$runningUnder" == "cygwin" || "$runningUnder" == "mingw" ]]; then
        if [[ "${USE_WSLG}" == "1" ]]; then
            FORCE_EXEC_DOCKER_INSIDE_WSL=1
            PULSE_SERVER="$(wsl --distribution ${WSL_INTEROP_DISTRO} -- echo '${PULSE_SERVER}')"
        else
            PULSE_SERVER="tcp:$(/c/Windows/System32/route.exe print | grep 0.0.0.0 | head -1 | awk '{print $4}'):0.0"
        fi
    else
        PULSE_SERVER=":1"
    fi
fi

if [[ "${DOCKER_CONTAINER_MOUNT_DRIVES}" == "1" ]]; then
    if [[ "$runningUnder" == "cygwin" || "$runningUnder" == "mingw" ]]; then
        DRIVE_LETTERS="$(wmic logicaldisk get caption | tail +2 | xargs | sed 's/://g' | tr ' ' '\n')"
        MOUNT_STRING=""
        for DRIVE_LETTER in ${DRIVE_LETTERS}; do
            LOWER_DRIVE_LETTER="$(echo "${DRIVE_LETTER}" | tr '[:upper:]' '[:lower:]')"
            if [[ "${FORCE_EXEC_DOCKER_INSIDE_WSL}" == "1" ]]; then
                MOUNT_STRING="${MOUNT_STRING} --volume /mnt/${LOWER_DRIVE_LETTER}:/mnt/${LOWER_DRIVE_LETTER}"
            else
                MOUNT_STRING="${MOUNT_STRING} --volume ${DRIVE_LETTER}:\\:/mnt/${LOWER_DRIVE_LETTER}"
            fi
        done
    else
        MOUNT_STRING=" --volume /:/mnt/host"
    fi
else
    MOUNT_STRING=""
fi
DOCKER_CONTAINER_EXTRA_ARGS="${DOCKER_CONTAINER_EXTRA_ARGS} ${MOUNT_STRING}"

if [[ ! -z ${DOCKER_CONTAINER_ENVFILE+x} && -f "${DOCKER_CONTAINER_ENVFILE}" ]]; then
    if [[ "${FORCE_EXEC_DOCKER_INSIDE_WSL}" == "1" ]]; then
        RPTMP="$(realpath ${DOCKER_CONTAINER_ENVFILE})"
        DOCKER_CONTAINER_EXTRA_ARGS="${DOCKER_CONTAINER_EXTRA_ARGS} --env-file \"/mnt${DOCKER_CONTAINER_ENVFILE}\""
    else
        DOCKER_CONTAINER_EXTRA_ARGS="${DOCKER_CONTAINER_EXTRA_ARGS} --env-file \"${DOCKER_CONTAINER_ENVFILE}\""
    fi
fi

if [[ -z ${DOCKER_CONTAINER_OUTSIDE_INCLUDE_FILE+x} ]]; then
    DOCKER_CONTAINER_OUTSIDE_INCLUDE_FILE=~/asbru-cm-docker-include-on-wrapper.sh
fi
if [[ ! -z ${DOCKER_CONTAINER_OUTSIDE_INCLUDE_FILE+x} && -f "${DOCKER_CONTAINER_OUTSIDE_INCLUDE_FILE}" ]]; then
    DOCKER_CONTAINER_ABSOLUTE_OUTSIDE_INCLUDE_FILE="$(realpath ${DOCKER_CONTAINER_OUTSIDE_INCLUDE_FILE})"
    DOCKER_CONTAINER_OUTSIDE_INCLUDE_FILE_WITH_MOUNT="/mnt${DOCKER_CONTAINER_ABSOLUTE_OUTSIDE_INCLUDE_FILE}"
    DOCKER_CONTAINER_EXTRA_ARGS="${DOCKER_CONTAINER_EXTRA_ARGS} --env OUTSIDE_INCLUDE_FILE=${DOCKER_CONTAINER_OUTSIDE_INCLUDE_FILE_WITH_MOUNT}"
fi

function createAndStart {
    OLDIFS="${IFS}"
    IFS=" "
    if [[ "${FORCE_EXEC_DOCKER_INSIDE_WSL}" == "1" ]]; then
        wsl --distribution "${WSL_INTEROP_DISTRO}" -- docker run --name "${DOCKER_CONTAINER_NAME}" --interactive --tty --env DISPLAY="${DISPLAY}" --env PULSE_SERVER="${PULSE_SERVER}" ${DOCKER_CONTAINER_EXTRA_ARGS} "${DOCKER_IMAGE_FULL_NAME}"
    else
        docker run --name "${DOCKER_CONTAINER_NAME}" --interactive --tty --env DISPLAY="${DISPLAY}" --env PULSE_SERVER="${PULSE_SERVER}" ${DOCKER_CONTAINER_EXTRA_ARGS} "${DOCKER_IMAGE_FULL_NAME}"
    fi
    IFS="${OLDIFS}"
}

function performActions {
    if [[ "${DOCKER_BUILD_IMAGE}" == "1" ]]; then
        echo "Building image '${DOCKER_IMAGE_FULL_NAME}'"
        OLDIFS="${IFS}"
        IFS=" "
        docker build "${DOCKERFILE_PATH}" --target "${DOCKER_IMAGE_TARGET}" --tag "${DOCKER_IMAGE_FULL_NAME}" --build-arg CHECKOUT_CACHE_BUST=$(date +%s) ${DOCKER_BUILD_EXTRA_ARGS}
        IFS="${OLDIFS}"
        echo "Built image '${DOCKER_IMAGE_FULL_NAME}'"
    fi
    if [[ "${DOCKER_START}" == "1" ]]; then
        CID="$(docker ps -aq -f name=^${DOCKER_CONTAINER_NAME}$)"
        if [[ "${CID}" ]]; then
            echo "Container exists. Name=${DOCKER_CONTAINER_NAME} Id=${CID}"
            if [[ "$(docker ps -aq -f status=exited -f name=^${DOCKER_CONTAINER_NAME}$)" ]]; then
                if [[ "${DOCKER_CONTAINER_PRESERVE}" == "1" ]]; then
                    if [[ "${DOCKER_START}" == "1" ]]; then
                        echo "Starting existing container"
                        docker start "${DOCKER_CONTAINER_NAME}"
                    fi
                else
                    echo "Recreating container"
                    docker rm "${DOCKER_CONTAINER_NAME}"
                    createAndStart
                fi
            elif [[ "$(docker ps -aq -f status=running -f name=^${DOCKER_CONTAINER_NAME}$)" ]]; then
                echo "Container is already running!"
                if [[ "${DOCKER_CONTAINER_PRESERVE}" == "1" ]]; then
                    echo "[TODO] Focusing container window"
                    # TODO: Focus the window, like a normal GUI application would
                    exit 1
                else
                    echo "Overwriting container!"
                    docker rm --force "${DOCKER_CONTAINER_NAME}"
                    createAndStart
                fi
            else
                echo "Container has unknown status! Status: TODO"
                exit 2
            fi
        else
            echo "Container does not exist, creating and running!"
            createAndStart
        fi
    fi
    if [[ "${DOCKER_PUSH}" == "1" ]]; then
        TAGSTRING="${DOCKER_REGISTRY}/${DOCKER_REPOSITORY}:${DOCKER_IMAGE_TAG}"
        docker tag "${DOCKER_IMAGE_FULL_NAME}" "${TAGSTRING}"
        docker login --username "${DOCKER_USERNAME}" --password "${DOCKER_PASSWORD}" "${DOCKER_REGISTRY}"
        docker push "${TAGSTRING}"
    fi
}

performActions
