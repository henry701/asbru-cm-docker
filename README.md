# Docker Build for asbru-cm

Dockerfile and other resources for building [asbru-cm](https://www.asbru-cm.net/) image on top of Alpine Linux, and tools for running it as hassle-free as possible on Windows.
