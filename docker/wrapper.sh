#!/bin/bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/ ;)
set -euo pipefail
IFS=$'\n\t'

# Too much output can't hurt, it's Bash.
set -x

OUTSIDE_INCLUDE_FILE="${OUTSIDE_INCLUDE_FILE-}"

if [[ ! -z ${OUTSIDE_INCLUDE_FILE+x} && -f "${OUTSIDE_INCLUDE_FILE}" ]]; then
    # Remove carriage returns
    sed -i 's/\r$//' "${OUTSIDE_INCLUDE_FILE}"
    source "${OUTSIDE_INCLUDE_FILE}"
fi

ASBRU_CONF_DIR_BASE="${ASBRU_CONF_DIR_BASE-/var/opt/asbru-cm/config}"
ASBRU_CONF_DIR_PROFILE="${ASBRU_CONF_DIR_PROFILE-default}"
ASBRU_CONF_FULL_DIR="${ASBRU_CONF_FULL_DIR:="${ASBRU_CONF_DIR_BASE}/profiles/${ASBRU_CONF_DIR_PROFILE}"}"

mkdir -p "${ASBRU_CONF_FULL_DIR}"

CONFIG_WRAP_DIR="${CONFIG_WRAP_DIR-}"
CONFIG_INIT_DIR="${CONFIG_INIT_DIR:=${CONFIG_WRAP_DIR}}"
CONFIG_BACKUP_DIR="${CONFIG_BACKUP_DIR:=${CONFIG_WRAP_DIR}}"

if [[ ! -z ${CONFIG_INIT_DIR+x} && -f "${CONFIG_INIT_DIR}" ]]; then
    rm -rf "${CONFIG_INIT_DIR}" || true
    cp --archive "${ASBRU_CONF_FULL_DIR}" "${CONFIG_BACKUP_DIR}" || true
fi

EXIT_CODE=0
./asbru-cm --config-dir="${ASBRU_CONF_FULL_DIR}" $@ || EXIT_CODE=$?

if [[ DEBUG_SLEEP_ON_EXIT ]]; then
    while :; do sleep 10000; done
fi

if [[ ! -z ${CONFIG_BACKUP_DIR+x} && -f "${CONFIG_BACKUP_DIR}" ]]; then
    rm -rf "${CONFIG_BACKUP_DIR}" || true
    cp --archive "${ASBRU_CONF_FULL_DIR}" "${CONFIG_BACKUP_DIR}" || true
fi

exit $EXIT_CODE
